package stumpkevin.lab3;

        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.support.v7.app.AppCompatActivity;
        import android.util.Log;
        import android.view.View;
        import android.widget.EditText;
        import android.widget.TextView;
        import android.widget.Button;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.BufferedInputStream;
        import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.net.HttpURLConnection;
        import java.net.MalformedURLException;
        import java.net.URL;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    TextView textView;
    String  inputString;
    String url = "http://date.jsontest.com";
    String result = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goClick (View v){
        editText = (EditText) findViewById(R.id.editText);
        inputString = editText.getText().toString();

        //This is how you execute an asynctask. Create a new
        //instance of the async class and call execute().
        new sendPostRequest().execute(url);
    }


    public class sendPostRequest extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            //Now in a background thread. Do all work with the URL
            //here.

            HttpURLConnection connection = null;
            //You will need some String variables
            String data = "";

            try {
                //grab url from the first param and instantiate a new URL object
                URL url = new URL(params[0]);

                //attempt to open the connection
                connection = (HttpURLConnection) url.openConnection();

                //grab the present response code
                int code = connection.getResponseCode();

                //if the response code is ready (200), proceed to processing the return data
                if (code == 200) {

                    //get input stream handle
                    InputStream input = new BufferedInputStream(connection.getInputStream());

                    //if input contains something
                    if (input != null) {
                        //get a buffered reader
                        BufferedReader bReader = new BufferedReader(new InputStreamReader(input));

                        //clear the return data string
                        StringBuilder sbuilder = new StringBuilder();
                        data = "";
                        
                        //bReader.ready() returns false when the buffer is emptied
                        while (bReader.ready()) {
                            //append a single character at a time to the data string
                            sbuilder.append(bReader.readLine());
                        }
                        //close the input stream
                        input.close();
                        data = sbuilder.toString();
                    }
                }
                //error catchers
            }catch(MalformedURLException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();

                //close the connection when done
            }finally{
                connection.disconnect();
            }

            //Generate a new JSONObject with the data read in
            //from the BufferedReader.
            try {
                Log.d("DATA CONTENTS: ", data );
                JSONObject jsonObject = new JSONObject(data);
                result = jsonObject.getString(inputString);
            } catch(JSONException e) {
                e.printStackTrace();
            }
            return result; //You are returning the result to the onPostExecute method following below.
        }

        @Override
        protected void onPostExecute(String result) {
            TextView resultText = (TextView) findViewById(R.id.textView4);
            resultText.setText(result);

            //This is where you will display the String you got
            //from the JSONObject in the text field on the app.
        }
    }
}


